//
//  ReceivingVC.swift
//  DelegateMayhem
//
//  Created by Caleb Stultz on 8/20/16.
//  Copyright © 2016 Caleb Stultz. All rights reserved.
//

import UIKit

class ReceivingVC: UIViewController, DataSentDelegate {

    @IBOutlet weak var receivingLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    func userDidEnterData(data: String) {
        receivingLabel.text = data
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showSendingVC" {
            let sendingVC: SendingVC = segue.destinationViewController as! SendingVC
            sendingVC.delegate = self
        }
    }
    
}

